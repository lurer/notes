# Mac utils

### OS Tweaks
ZSH
Oh-my-zsh
Little Snitch
Magnet
Tunnelblick
ExpanDrive

### Notes / Docker
Boostnote
GNS3
Visual Studio Code
Termius 
Docker Captian

### Misc Utils
Etcher
Affinity Photo
Zenmap
