# Python CentOS #

**CENTOS PYTHON3 DEV BOX**
```
yum update -y
yum upgrade -y

yum groupinstall -y "development tools"

yum install -y \
  libffi-devel \
  git \
  curl \
  wget \
  unzip \
  ftp \
  zlib-devel \
  bzip2-devel \
  openssl-devel \
  ncurses-devel \
  sqlite-devel \
  readline-devel \
  tk-devel \
  gdbm-devel \
  db4-devel \
  libpcap-devel \
  xz-devel \
  expat-devel \
  yum install -y \
  lsof \
  wget \
  vim-enhanced \
  zsh \
  words \
  which
```
**INSTALL OH MY ZSH**
```
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```
*Change shell to ZSH*
```
chsh -s /usr/bin/zsh
```
**SET TIME TO EDT**
```
rm -rf /etc/localtime
ln -s /usr/share/zoneinfo/America/New_York /etc/localtime
```
**INSTALL PYTHON**
```
cd /usr/src
https://www.python.org/downloads/ --DOWNLOAD LATEST--
tar xvf Python-3.7.0.tar.xz
cd Python-3.7.0
./configure --enable-optimizations
make altinstall
exit
```
**EDIT SUDOERS FILE TO INCLUDE NEW PATH**
```
vim /etc/sudoers
```
**Ensure that secure_path in /etc/sudoers file includes /usr/local/bin. The line should look something like this:**
``Defaults    secure_path = /sbin:/bin:/usr/sbin:/usr/bin:/usr/local/bin``

```
which python3.7
/usr/local/bin/python3.7
```

